/**
 * @author Rohit Ranjan
 * This is a singleton, used to generate invoice on console.
 */

package com.rohit.basket;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.rohit.factory.ProductBuilderFactory;
import com.rohit.model.ItemDetails;
import com.rohit.model.Product;

public enum InvoiceGenerator {
	
	INSTANCE;
	
	private static String[] keywords;
	
	//static initializer block to load all the keywords from keywords.xml file using DOM parser
	static{
		try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse("keywords.xml");
				
				doc.getDocumentElement().normalize();
			 
				NodeList nList = doc.getElementsByTagName("keywords");
				keywords = nList.item(0).getTextContent().split(",");
				
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
	}
	
	/**
	 *  
	 * @param file
	 */
	public void generateInvoice(String file){
		
		BufferedReader br = null;
		FileInputStream ir = null;
		Map<String, List<String>> inputs = new HashMap<String, List<String>>();
		String inputType = null;
		List<String> inputItems = null;
		try {
			ir = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(ir));
			String line = br.readLine();
			while(line != null){
				if(!line.trim().isEmpty()){
					Pattern patt = Pattern.compile("Input??");
					Matcher mat = patt.matcher(line);
					if(mat.find()){
						inputType = line;
						inputItems = new ArrayList<String>();
						line = br.readLine();
						
					}
					inputItems.add(line);
					inputs.put(inputType, inputItems);
					
				}
				line = br.readLine();
				}
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String, List<ItemDetails>> itemDetails = buildItems(inputs);
		displayInvoice(itemDetails);
	}
	
	
	/**
	 * This method display the Invoice on console
	 * @param inputs
	 */
	private void displayInvoice(Map<String, List<ItemDetails>> inputs){
		DecimalFormat df = new DecimalFormat("###.##");
		for(Map.Entry<String,List<ItemDetails>> pairs : inputs.entrySet()){
			double sum = 0;
			System.out.println("-------------------------------------------------------------------\n");
			System.out.println(pairs.getKey());
			System.out.println("-------------------------------------------------------------------");
			System.out.printf("%-30s %-10s %-16s %-10s %n", "Product", "Quantity", "Price", "Tax");
			System.out.println("-------------------------------------------------------------------");
			for(ItemDetails items : pairs.getValue()){
				String productName = items.getProductName();
				int quantity = items.getQuantity();
				double price = items.getPricePerItem();
				String priceStr = df.format(price);
				double tax = items.getProductType().taxAmount();
				String taxStr = df.format(tax);
				System.out.printf("%-30s %-10d %-16s %-10s %n", productName, quantity, priceStr,taxStr);
				sum = sum + findTotal(quantity, price, tax);
			}
			String sumStr = df.format(sum);
			System.out.println("-------------------------------------------------------------------");
			System.out.printf("%-30s %-10s %-16s %n", "Total", "", sumStr);
		}
		System.out.println("-------------------------------------------------------------------\n");
	}
	
	/**
	 * This method create ItemDetails object.
	 * @param inputs
	 * @return
	 */
	private  Map<String, List<ItemDetails>> buildItems(Map<String, List<String>> inputs){
		Map<String, List<ItemDetails>> itemDetails = new HashMap<String, List<ItemDetails>>();
		for(Map.Entry<String,List<String>> pairs : inputs.entrySet()){
			List<ItemDetails> items = new ArrayList<ItemDetails>();
			for(String str : pairs.getValue()){
				ItemDetails item = new ItemDetails();
				String[] strs = str.split("@");
				String itemStr = strs[0];
				int quantity = Integer.parseInt(itemStr.substring(0,1).trim());
				String productName = itemStr.substring(1).trim();
				double price = Double.parseDouble(strs[1].trim());
				item.setProductName(productName);
				item.setQuantity(quantity);
				item.setPricePerItem(price);
				Product prod = ProductBuilderFactory.INSTANCE.buildProduct(productName);
				item.setProductType(prod);
				items.add(item);
			}
			itemDetails.put(pairs.getKey(), items);
		}
		return itemDetails;
	}
	
	/**
	 * This method calculates the total bill amount
	 * @param quantity
	 * @param price
	 * @param tax
	 * @return
	 */
	private double findTotal(int quantity, double price, double tax){
		double total = quantity*(price*(1.0 + (tax/100.0)));
		return total;
	}
	
	public String[] getKeywords() {
		return keywords;
	}

	public void setKeywords(String[] keywords) {
		InvoiceGenerator.keywords = keywords;
	}

}
