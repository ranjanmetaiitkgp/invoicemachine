/**
 * @author Rohit Ranjan
 * 
 * Main program, takes filePth as argument. eg. /Users/rohit/Documents/InvoiceTester/basket.txt
 * file "basket.txt" provides the input to the InvoiceGenerator
 * 
 */

package com.rohit.main;

import com.rohit.basket.InvoiceGenerator;

public class MainProgram {

	public static void main(String[] args){
		String file = args[0];
		InvoiceGenerator.INSTANCE.generateInvoice(file);
	}
}
