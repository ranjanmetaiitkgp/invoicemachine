/**
 * @author Rohit Ranjan
 * Taxable interface to return tax amount.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.strategy;

public interface Taxable {

	double tax();
}
