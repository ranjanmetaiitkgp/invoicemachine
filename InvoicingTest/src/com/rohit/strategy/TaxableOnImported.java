/**
 * @author Rohit Ranjan
 * TaxableOnImported class implements Taxable interface to return 2.4 as the default taxable amount.
 * This class provides the facility to set the final taxable amount with the help of setter method.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.strategy;

public class TaxableOnImported implements Taxable{

	private double taxAmount;
	
	public TaxableOnImported(){
		taxAmount = 2.4;
	}
	
	@Override
	public double tax() {
		// TODO Auto-generated method stub
		return taxAmount;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

}
