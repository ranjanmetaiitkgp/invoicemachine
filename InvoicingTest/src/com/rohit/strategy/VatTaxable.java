/**
 * @author Rohit Ranjan
 * VatTaxable class implements Taxable interface to return 12.5 as the taxable amount.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.strategy;

public final class VatTaxable implements Taxable{

	private double taxAmount;
	
	public VatTaxable(){
		taxAmount = 12.5;
	}
	
	@Override
	public double tax() {
		// TODO Auto-generated method stub
		return taxAmount;
	}

}
