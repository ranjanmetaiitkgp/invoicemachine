/**
 * @author Rohit Ranjan
 * NonTaxable class implements Taxable interface to return 0.0 as the taxable amount.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.strategy;

public final class NonTaxable implements Taxable{

	private double taxAmount;
	
	public NonTaxable(){
		taxAmount = 0.0;
	}
	
	@Override
	public double tax() {
		// TODO Auto-generated method stub
		return taxAmount;
	}

}
