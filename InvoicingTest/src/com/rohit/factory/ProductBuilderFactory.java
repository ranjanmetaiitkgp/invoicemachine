/**
 * 
 * @author Rohit Ranjan
 * This singleton is used to create taxable or non-taxable type of product based on keywords
 * This uses Factory DP to create Product object
 * 
 */
package com.rohit.factory;

import com.rohit.basket.InvoiceGenerator;
import com.rohit.model.NonVatTaxableProduct;
import com.rohit.model.Product;
import com.rohit.model.VatTaxableProduct;
import com.rohit.strategy.TaxableOnImported;

public enum ProductBuilderFactory {
	
	INSTANCE;
	
	/**
	 * Checks whether the keyword is found in the product name or not.
	 * Returns false if keyword is not found and true if the same is found.
	 * @param productName
	 * @return
	 */
	private boolean isKeywordFound(String productName){
		String[] keywords = InvoiceGenerator.INSTANCE.getKeywords();
		for(int i =0; i<keywords.length;i++){
			if(productName.toLowerCase().contains(keywords[i].toLowerCase())){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks whether the product is imported or not on the basis of presence of "imported" keyword in the product name.
	 * @param productName
	 * @return
	 */
	public boolean isImported(String productName){
		if(productName.toLowerCase().contains("imported")){
			return true;
		}
		return false;
	}
	
	/**
	 * Builds product by checking whether the product is taxable or not and imported or not.
	 * @param productName
	 * @return
	 */
	public Product buildProduct(String productName){
		Product  prod = null;
		TaxableOnImported newTaxable = null;
		if(!isKeywordFound(productName)){
			prod = new VatTaxableProduct();
			if(isImported(productName)){
				newTaxable = new TaxableOnImported();
				double vat = prod.taxAmount();
				double additionalTax = newTaxable.getTaxAmount();
				newTaxable.setTaxAmount(vat + additionalTax);
				prod.additionalTaxAbility(newTaxable);
			}
			return prod;
		}
		else{
			prod = new NonVatTaxableProduct(); 
			if(isImported(productName)){
				newTaxable = new TaxableOnImported();
				prod.additionalTaxAbility(newTaxable);
			}
			return prod;
		}
	}

}
