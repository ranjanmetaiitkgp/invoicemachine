/**
 * @author Rohit Ranjan
 * Model class for all types of NonVatTaxableProduct.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */

package com.rohit.model;

import com.rohit.strategy.NonTaxable;

public class NonVatTaxableProduct extends Product{

	public NonVatTaxableProduct(){
		super();
		taxable = new NonTaxable();
	}
}
