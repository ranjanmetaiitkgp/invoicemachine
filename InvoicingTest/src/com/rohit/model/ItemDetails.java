/**
 * @author Rohit Ranjan
 * POJO class to store information about the product details.
 */
package com.rohit.model;

public class ItemDetails {

	private int quantity;
	private String productName;
	private double pricePerItem;
	private Product productType;
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getPricePerItem() {
		return pricePerItem;
	}
	public void setPricePerItem(double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}
	public Product getProductType() {
		return productType;
	}
	public void setProductType(Product productType) {
		this.productType = productType;
	}
}
