/**
 * @author Rohit Ranjan
 * Model class for all types of VatTaxableProduct.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.model;

import com.rohit.strategy.VatTaxable;

public class VatTaxableProduct extends Product{
	
	public VatTaxableProduct(){
		super();
		taxable = new VatTaxable();
	}

}
