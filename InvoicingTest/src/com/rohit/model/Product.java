/**
 * @author Rohit Ranjan
 * Super Model class for all types of products.
 * Uses Strategy DP to decide the taxable amount at runtime.
 */
package com.rohit.model;

import com.rohit.strategy.Taxable;

public class Product {
	
	protected Taxable taxable;
	
	public double taxAmount(){
		return taxable.tax();
	}
	
	public void additionalTaxAbility(Taxable newTaxable){
		taxable = newTaxable;
	}
	

}
